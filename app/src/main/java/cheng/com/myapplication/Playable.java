package cheng.com.myapplication;

public interface Playable {

    void onTrackPrevious();
    void onTrackPlay();
    void onTrackPause();
    void onTrackNext();
}
